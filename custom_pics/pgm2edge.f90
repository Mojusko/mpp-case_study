module ipgmio

implicit none

integer, parameter, private :: iounit = 10
integer, parameter :: thresh = 255

contains


!  Routine to get size of a PGM image
!  Note that this assumes no comments and no other white space
!  You can create PGMS using:
!  convert image.jpg -colorspace gray -compress none -depth 8 image.pgm 
!  then edit them slightly.

subroutine ipgmsize(filename, nx, ny)

  integer :: nx, ny

  character*(*) :: filename

  open(unit=iounit, file=filename)

  read(iounit,*)
  read(iounit,*) nx, ny

  close(unit=iounit)

end subroutine ipgmsize


subroutine ipgmread(filename, x)

  character*(*) :: filename
  integer :: nx, ny, nxt, nyt
  integer, dimension(:,:) :: x

  integer i, j

  nx = size(x,1)
  ny = size(x,2)

  write(*,*) 'Reading ', nx, ' x ', ny, ' picture from file: ', filename

  open(unit=iounit, file=filename)

  read(iounit,*)
  read(iounit,*) nxt, nyt

  if (nx .ne. nxt .or. ny .ne. nyt) then
    write(*,*) 'pgmread: size mismatch, (nx,ny) = (', nxt, ',', nyt, &
               ') expected (', nx, ',', ny, ')'
    stop
  end if

  read(iounit,*)
  read(iounit,*) ((x(i,ny-j+1), i=1,nx), j=1,ny)

  close(unit=iounit)

end subroutine ipgmread


subroutine ipgmwrite(filename, x)

  implicit none

  character*(*) :: filename
  integer, dimension(:,:) :: x

  integer :: i, j, nx, ny

  nx = size(x,1)
  ny = size(x,2)

  write(*,*) 'Writing ', nx, ' x ', ny, ' picture into file: ', filename

  open(unit=iounit, file=filename)

  write(iounit,fmt='(''P2''/''# Written by ipgmwrite'')')
  write(iounit,fmt='(i4, '' '', i4)') nx, ny

  write(iounit,fmt='(i4)') thresh
  write(iounit,fmt='(12('' '', i5))') ((x(i, ny-j+1) , i=1,nx), j=1,ny)

  close(unit=iounit)

end subroutine ipgmwrite

end module ipgmio


program pgm2edge

  use ipgmio

  implicit none

  integer, allocatable, dimension(:,:) :: image, edge

  integer, parameter :: maxlen = 32

  character*(maxlen) :: infile, outfile

  integer :: i, j, nx, ny, imax, imin

  if (command_argument_count() /= 2) then
     write(*,*) 'Usage: pgm2edge <input image file> <output edge file>'
     stop
  end if

  call get_command_argument(1, infile)
  call get_command_argument(2, outfile)

  infile  = trim(infile)
  outfile = trim(outfile)
  
  write(*,*) 'pgm2edge: infile = ',  infile, 'outfile = ', outfile

  call ipgmsize(infile, nx, ny)

  write(*,*) 'nx, ny = ', nx, ny

  allocate(image(nx,ny))
  allocate(edge(nx,ny))

  call ipgmread(infile, image)
  
  write(*,*) 'pgmread: image min, max = ', minval(image(1:nx, 1:ny)), &
                                           maxval(image(1:nx, 1:ny))

  do j = 2,ny-1
    do i = 2, nx-1

      edge(i,j) = image(i+1,j) + image(i-1,j) + image(i,j+1) + image(i,j-1) &
                  - 4.0*image(i,j)

    end do
  end do

  do i = 2, nx-1

    edge(i,1)  = image(i+1,1) + image(i-1,1) + image(i,2) + thresh &
                 - 4.0*image(i,1)

    edge(i,ny) = image(i+1,ny) + image(i-1,ny) + image(i,ny-1) + thresh &
                 - 4.0*image(i,ny)

  end do

  do j = 2, ny-1

    edge(1,j) =  image(2,j) + image(1,j+1) + image(1,j-1) + thresh &
                 - 4.0*image(1,j)

    edge(nx,j) = image(nx-1,j) + image(nx,j+1) + image(nx,j-1) + thresh &
                 - 4.0*image(nx,j)

  end do

  edge(1,1)   = image(2,1)     + image(1,2)      + 2*thresh - 4.0*image(1,1)
  edge(1,ny)  = image(2,ny)    + image(1,ny-1)   + 2*thresh - 4.0*image(1,ny)
  edge(nx,1)  = image(nx,2)    + image(nx-1,1)   + 2*thresh - 4.0*image(nx,1)
  edge(nx,ny) = image(nx-1,ny) + image(nx, ny-1) + 2*thresh - 4.0*image(nx,ny)

  call ipgmwrite(outfile, edge)

  deallocate(image)
  deallocate(edge)

  write(*,*) 'pgm2edge: finished'

end program pgm2edge
