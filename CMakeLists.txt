project(biglearning)
cmake_minimum_required (VERSION 2.6)

IF(NOT CMAKE_BUILD_TYPE)
	SET(CMAKE_BUILD_TYPE Release CACHE STRING "" FORCE)
ENDIF()

find_package(MPI REQUIRED)
include_directories(${MPI_INCLUDE_PATH})

ADD_LIBRARY(pgmio SHARED
	src/pgmio.h
	src/pgmio.c
	src/send.h
	src/send.c
	src/help.h
	src/help.c
	src/parameters.h
	src/parameters.c
	)


add_executable(serial
	src/main.c
)

add_executable(parallel
	src/parallel.c
	)

add_executable(test2
	src/test2.c
	)

target_link_libraries(pgmio ${MPI_C_LIBRARIES})
target_link_libraries(serial pgmio)
target_link_libraries(serial ${MPI_C_LIBRARIES})
target_link_libraries(parallel pgmio)
target_link_libraries(parallel ${MPI_C_LIBRARIES})
target_link_libraries(test2 pgmio)
target_link_libraries(test2 ${MPI_C_LIBRARIES})