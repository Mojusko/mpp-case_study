\documentclass[11pt]{article}
\usepackage[parfill]{parskip}
\usepackage{listings}
\usepackage{color}
\usepackage{gensymb}
\usepackage{fancyhdr} % for headers and footers
\usepackage{graphicx}
\usepackage{framed}
\usepackage{hyperref}
\usepackage{amsfonts}
\usepackage{epstopdf}
\usepackage{mathtools}
%\usepackage[bottom=4em]{geometry}
\usepackage[top=1.0in, bottom=0.75in,left=0.7in,right=0.7in]{geometry}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\usepackage{amsmath}
\usepackage{amsthm}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\usepackage{mathrsfs}


\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=C,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true
  tabsize=3
}

%-------------------------------

\title{Message-Passing Programming - Course Work \\ Image Reconstruction from its Laplacian in Parallel using MPI 2D decomposition}
\author{	Mojmir Mutny,\\
            The University of Edinburgh
}
\date{November 25, 2014}

\begin{document}
	
\maketitle

\fancyhead[R]{ MPP - Course Work }
\pagestyle{fancy}
	
\section{Introduction}
In this report we present a theoretical background behind a parallel implementation of image reconstruction algorithm. In addition to that, we include useful instruction for running the practical implementation of the image reconstruction code that accompanies this report. Experimental results along with proofs of correctness of the practical implementation are also present in this report.
\section{Design}
\subsection{Task and Serial Code}
The algorithm aims to solve inverse problem of Laplacian convolution. Initially, we are given image convoluted with a kernel \ref{Kernel}.
\begin{equation}
\label{Kernel}
\mathcal{K}=\frac{1}{4}
\begin{pmatrix}
0 & 1 & 0 \\
1 & -4 & 0 \\
0 & 1 & 0 
\end{pmatrix}
\end{equation}
This cause images to look like the image in Figure \ref{Images} i.e. their their edges are enhanced. This procedure is well known in basic image processing due to its very good properties of enhancing edges. In our coursework, we want to perform the inverse operation which is noted with red arrow in Figure \ref{Images}, i.e. this means to reconstruct imaged from its edge image. This operation is possible, and has unique solution given boundary conditions. A simple serial algorithm that recovers the image can be formulated as follows:
\begin{figure}
	\centering
	\includegraphics[width=14cm,height=5cm]{images.jpg}
	\caption{Convolution with Kernel \ref{Kernel} is operation represented by the black arrow in the image. The inverse operation that we aim to perform is represented with the red arrow}
	\label{Images}
\end{figure}

\begin{framed}
	\textbf{Simple Serial Algorithm}
\begin{enumerate}
	\item Initialize \textsc{edge},\textsc{old},\textsc{new} as [N+1][M+1] arrays
	\item Load Edge Image of size [M][N] to \textsc{edge} with boundaries equal to $255$ 
	\item Set all elements in \textsc{old},\textsc{new} to $255$
	\item Iterate over k
	\begin{enumerate}
		\item Iterate over Image $i,j$ 
 \begin{equation}
\text{\textsc{new}}_{i,j}=\frac{1}{4}(\text{\textsc{old}}_{i-1,j}+\text{,\textsc{old}}_{i+1,j}+\text{\textsc{old}}_{i,j-1}+\text{\textsc{old}}_{i,j+1}-\text{\textsc{edge}}_{i,j})
\label{update}
\end{equation}
	\item $\text{\textsc{old}}=\text{\textsc{new}}$
	\end{enumerate} 
	\item Until happy
	\item Output \textsc{old} without boundary halos	
\end{enumerate}	
\end{framed}

Our goal in this coursework is to parallelise this code so that it can run on more processors and can be decomposed in 2D fashion. This means schematically \ref{pic} that each processor takes a part of the image, and does its iteration on this part. By the iteration steps we mean performing the calculation in the equation \ref{update} in the above description. The algorithm used only values of neighbouring pixels in order to make a single \ref{update} update in one iteration. As there are pixels whose neighbours are located on different processors due to decompositions, we have to take care of the halo exchanges as shown in \ref{pic}. However a good feature of this algorithm is that in between halo exchanges the program can iterate over the parts of the image where no halo information is required. This feature is captured in the implementation. 

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{picture.png}
	\caption{Two dimensional decomposition with shown halo exchanges for node with coordinates (1,1). Also, in this image one can see the definition of padn,padm,padn\_max,padn\_max,startm,startn. Startm and startn are shown for rank (0,1) as well as are mm and nn, which correspond to size of local dataset on a node. Padn and other variables are introduced in order to not iterate over blank boundaries of the image. Some of the nodes like for example (0,0) have non-zero padm,padn values while e.g. node (1,1) has all padding variables equal to zero}
	\label{pic}
\end{figure}

\subsection{Theory of Parallel Implementation}
In this section, I would like to describe the design of the algorithm I implemented. Firstly, I would like to note a few conventions that were used when writing this program:
\begin{enumerate}
\item The Rank 0 is reserved for the master node
\item Tags in message passing were not used, $defaulttag = 0$ everywhere
\item Neighbours of a pixel in a grid are enumerated 0 =  left, 1 = up, 2 = right, 3 = down
\end{enumerate}

Secondly, as mentioned in previous section the image reconstruction algorithm is a local algorithm. In particular, this means that to update a pixel in an image, the values of its neighbours and edge informations are needed only. 

\subsection{Basic Description}
Here we present short overview of what is happening in our implementation of the parallel code. 
\begin{framed}
\textit{\textbf{Initialization \& Data Transfer}}
\begin{enumerate}
\item Loading of parameters from an input file 
\item Creation of 2D topology for our decomposition, using functions \textsc{MPI\_Dims\_create} and \textsc{MPI\_Cart\_create}
\end{enumerate}
\begin{description}
\item[Master] \hfill
\begin{enumerate}
\item Probe the dimension of the edge image that is being reconstructed. The func. returns $(N,M)$
\item Broadcast these dimensions to all other worker nodes $(N+1,M+1)$
\item Read the data of the edge image to \textsc{edge}	
\item Add white halos at the ends of the edge image \textsc{edge} making it of size $[M+1][N+1]$
\item Distribute parts of the edge image \textsc{edge} to the worker nodes 
\item Calculate coordinates and part of the image that is operated on master node $(mm,nn)$ using function \textsc{Optimal\_Splitting}
\item Splice out part of the image operated on master
\end{enumerate}
\item[Worker] \hfill
\begin{enumerate}
\item Receive dimensions of edge image from the master node $(N+1=NN,M+1=MM)$
\item We calculate the size of the patch $(mm,nn)$, and position $(startm,startn)$ of the patch that this node will operate on using function \textsc{Optimal Splitting}
\item Receive data from master node of the size $[mm][nn]$ calculated above
\end{enumerate}
\end{description}
\textit{\textbf{Calculation}}
\begin{enumerate}
\item Create array \textsc{old}, \textsc{new} with dimensions $[mmh=mm+2,nnh=nn+2]$
\item Set all elements in arrays \textsc{old}, \textsc{new} to $255$.
\item Check for neighbours of this node
\item Check For Image Padding. This function returns 4 numbers $(padm,padn,padm\_max,padn\_max)$
\item Iterate
\begin{enumerate}
\item Set $\Delta=0$
\item Send Halos of the image with non-blocking send and derived datatypes
\item Iterate over non-halo part of the image as in \ref{update}, update $\Delta$
\item Receive Halos (blocking receive) to a derived datatype
\item Wait for the non-blocking halos to be send
\item Iterate over halo part of the image as in  \ref{update}, update $\Delta$
\item Copy: \textsc{new} = \textsc{old}
\item Calculate Average of the image every \textsc{update\_frequency} times
\end{enumerate}
Until Happy (EITHER $iteration>iteration_{max}$ OR $\Delta<\Delta_{max}$)
\end{enumerate}
\textit{\textbf{Collecting}}
\begin{description}
\item[Worker] \hfill
\begin{enumerate}
	\item Send back to master node \textsc{OLD} without the border halos
\end{enumerate}
\item[Master] \hfill
\begin{enumerate}
	\item Receive image patches from worker nodes and incorporate them to a big array
	\item Add image patch operated on master
	\item Cut Edge of the image and output to a file. 
\end{enumerate}
\end{description}

\end{framed}
\subsection{Additional Description}
In this subsection, I would like to note a few non-obvious features or programming choices I made in developing this implementation. For example in the initialization phase we calculate size of the patch with function \textsc{Optimal Splitting}. This function can handle any topology, and any possible decomposition. If the decomposition is not homogeneous, it makes the nodes on the leftmost or uppermost part of the grid to operate on bigger patches. 

Also, due to implementation we chose as distributing the edge image with boundaries, we have to calculate where does the actual image starts (padding of the image). It can be seen in the Figure \ref{pic} that for example node (0,0) has non-zero padding while (1,1) has no borders and iteration can start at first pixel. This is done with the routine \textsc{Check\_Padding}. This is done only once at the beginning of Calculation phase. 

In addition to this, at the beginning of the calculation phase, neighbour ranks are calculated, and stored in an array for the rest of the runtime. Furthermore, calculation of $\Delta$ and Average is done only every \textsc{Update\_Frequency} iterations. This can be configured from an input file. 
\subsection{Code Organization} 
The code can be found in src/ subdirectory of the main file. This file contains 11 files. The content of the files is organized as follows:
\begin{enumerate}
	\item \textbf{help.h \& help.c} - These files contain basic help functions such as functions performing \ref{update}, average function and array handling functions
	\item \textbf{send.h \& send.c} - These files contain communication routines, functions include data distribution and halo sending. 
	\item \textbf{pgmio.h \& pgmio.c}  - File for I/O on *.pgm files
	\item \textbf{parameters.h \& parameters.c} - The *.h file contains parameters for the program such as compiler pragmas for modes, and one function for I/O loading of parameters
	\item \textbf{main.c} - This the simple serial implementation of the code.
	\item \textbf{parallel.c} - This is the general purpose implementation of the parallel code.
	\item \textbf{test2.c} - This is in essence same code as in parallel.c but the output is slightly changed for purpose of performance tests.
\end{enumerate}
\begin{figure}
\centering
\begin{tabular}{| l | c | r |}
		\hline
		No. & Name & Size \\ \hline
		1. & edge\_192x128.pgm & 192x128 \\ \hline
		2. & edge\_512x384.pgm & 512x384 \\ \hline
		3. & edge\_768x768.pgm & 768x768 \\ \hline
		4. & edge\_success2550x1695 & 2550x1695 \\ \hline
\end{tabular}
\caption{Test files used. They are located in /data subdirectory}
\label{table}
\end{figure}


\section{Demonstration of Correctness}
\begin{description}
\item[Comparison with the serial code]\hfill \\
A practical consequence of our implementation is that its iterations are equivalent to the serial code iterations. The only difference is that the computation of a single iteration is distributed over more nodes. Therefore, we should be able to compare the code with serial program, and see whether they have a same outputs after small fixed number of iterations $N$.

In order to perform this test, we used 3 images of different sizes in table  (first 3) \ref{table}, and each was run with $1 \hdots 17$ processors, and different number of iterations, in particular $N\in\{100,1000\}$. Afterwards, one of the oldest code in the history of UNIX "diff" was used to see whether there was a difference in the images generated by the two programs.

For purposes of testing this feature an automatized bash script which creates appropriate input files for the program and submits automatically jobs to Morar has been created. If you want to repeat this test then: set mode of the code to \#ITER, compile the source, and run the script test1.sh from the /test subdirectory (the program relies on the directory structure). After the jobs are finished you can go to subdirectory /results\_correctness of the main folder, where you find results from the test and a small bash script which performs, in essence, the diff command. Running this script should give an empty diff output if they match, if not then it outputs diff of the images from serial and parallel programs. Images, input files and job names have a time stamp in their name to distinguish them from others, and put order into running the program.

Each parallel output file was exactly the same as the serial code output file, which demonstrates that the code works correctly and in the case of 1 processor it coincides with the serial implementation.

\item[Float/Double swap] \hfill \\
In addition, swap between \textsc{float} and \textsc{double} was performed by setting constants in parameters.h file. The code has no problem running although the I/O routines supplied cannot handle arrays with doubles, so loading had to be preserved with floats. 

\item[Memory test] \hfill \\
We performed load test and memory test by running the algorithm with a very big image from the Table \ref{table}. If one wishes to rerun this test one can run test4.sh in test subdirectory with error mode of the program. 
\end{description}

\section{Performance Tests}

\subsection{Note on Performance tests}
When running performance tests one has to pick a good metric that can be used to compare the performance of different implementations and run modes. There is a huge possibility how one can compare algorithms, however usually the interesting metric boils down to the time they take to run. The same is the case with our program. Still, there are various metrics that could be used such as runtime of the whole algorithm or time per single iteration. When choosing it, we have to account for the fact that measuring operation which takes less than 1s is usually influenced by heavy noise, thus for such measurement statistical methods have to be used. 

To investigate the distribution of the time of single iteration, I have done a simple experiment where I have minimized the image768x768 with stopping condition $\Delta<0.001$. I did write out time to perform 100 iterations every 100 iterations, this was then rescaled to represent time for a single iteration. Distribution that was obtained can be seen in figure \ref{distribution}. One can immediately notice that the distribution has high peak at low times, but has very long tail in increasing direction of time. This behaviour is expected as there is very little (zero beyond some point) probability of being much faster, however one can experience significant reduction in iteration speed due to fluctuations in cluster load. As for our performance analysis the most important metric is how fast can one run the program we chose to take minimum value from this distribution for our measure instead of the mean value. The Mean value would not reflect the best possible performance that we are interested in, and would be biased by the long tail of the distribution.  
\begin{figure}
\centering
\includegraphics[scale=0.7]{distribution_size_20_768x768.eps}
\caption{Distribution of time per iteration in measurements. We can see that this distribution is not symmetric and has very long tails. The this was obtained by reconstructing image 768x768 with criterion $\Delta<0.001$}
\label{distribution}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.7]{no_average_performance.eps}
\caption{Here we present results of performance test where stopping criteria are given by $\Delta<0.001$ for different image sizes with various core involved. One can see that there is decreasing trend in time per iteration.}
\label{perresults}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.7]{speed_up.eps}
\caption{Here we present results of performance test where stopping criteria are given by $\Delta<0.001$ for different images. We show speed-up of the algorithm for each image. We see that with increasing size of the image the speed-up is more pronounced and consistent.}
\label{speedup}
\end{figure}

\subsection{Results \& Discussion}
The main performance test was performed with three images from Table \ref{table}, with stopping conditions $\Delta < 0.001$ on varying number of cores $(1 \hdots 24)$. The time per iteration can be seen in Figure \ref{perresults}, and speed-up results can be seen in \ref{speedup}. We calculated the time of a single iteration by taking the minimum of the rescaled time for 100 iterations over the whole runtime of the program (as noted in previous section).

We can see from the results that with increasing number of processors the performance increases for images 512x384 and 768x768, and time to run decreases. The speed-up for these two images is linear with slight deviation that could be due to fluctuation in cluster load or some decompositions may not be as favourable due to big number of communications. In case of the image 192x128 the performance increases linearly, but then sufferers fluctuations which can be seen best in the speed-up graph. This behaviour is expected as 192x128 is the smallest image used, and such behaviour for small images is expected due to trade-off between decomposition speed-up and communication costs.

With increasing number of processors we split the image to smaller and smaller pieces. It is natural to expect that there is a point at which performance instead of increasing decreases, because in one extreme operating on a single pixel, and communicating with 4 other neighbours at each iteration is comparably more computationally expensive than doing more iterations on a few pixels and then communicating. We can see that there is a point at which this trade-off breaks in case of the smallest image. Hint of this break down are also in the second smallest image. Obviously this happens at higher number of cores than in the smaller image. 

The wild fluctuations for the smallest image create an opportunity for an interesting discussion. This could be explained possibly due to more favourable decomposition given the size of the cluster. However, the intuition break down here because for example in the case of 23 processors (a prime number) corresponding to (23,1) decomposition we get worse performance than in 24 processors which corresponds to (6,4) decomposition although in the former there is less communications. This strange behaviour requires further investigation which we were unable to perform due to time constraints. We use default functions from MPI implementation to create our decompositions. These are not unique and others can be investigated. Interesting tests might include different decompositions with given number of processors such as for 24: (24,1), (12,2), (8,3) and (6,4). 

If one wishes to rerun the tests there is an automatized bash script which generates input files and submits jobs to Morar. Please set \#define ERROR mode of the program, compiler and run sh test3.sh within the test directory. The job output files contain the information needed using grep TIME command one can extract desired information. Possible grep keywords are TIME,MIN and RUN.

\section{Conclusion}
In this report we have analysed the theoretical design of a parallel image restoration algorithm with 2D decomposition using MPI. We have shown how to build and run our practical implementation. In addition to that, we have presented proofs that our program works correctly, and accompanied it with performance results which show that our program indeed achieves better performance in parallelised settings.
\section{Appendix}
\subsection{Requirements}
This code requires CMake version 2.8. You might be able to run it also with 2.6 provided you have FindMPI packages working correctly. One can download Cmake for free from \url{http://www.cmake.org/}. With 2.6 you can set links to libraries using ccmake.
\subsection{How to Build}
Building instructions depend on whether one uses gnu or pgi version of MPI \& Compiler. CMake has no problem detecting gnu-mpi with gnu compiler. If you want to run with pgi-mpi you have to add \textit{-DCMAKE\_C\_COMPILER=mpicc} option in the cmake command in the following list of operations. 
\begin{lstlisting}[language=bash]
cd repository
mkdir build
cd build
cmake ..
mpiexec -n 4 ./parallel < ../test_input_parallel.txt
./serial < ../test_input_serial.txt
\end{lstlisting}
\subsection{How to Run}
\begin{description}
\item[Program Modes] \hfill \\
The program has two running modes. Either one can run with constant number of iterations or set threshold with $\Delta_{max}$. This is configured by defining a compiler pragma in src/parameters.h. Either you \#define ITER or \#define ERROR, which correspond to previously mentioned modes respectively. 

Lastly, there is an option for verbosity. The code is required to print Average of the image at given iterations. The output to the screen of this average can be set by defining compiler pragma in src/parameters.h \#define VERBOSE.
\item[Input File] \hfill \\
One can see in the How to build section that the program requires an input file. This input file contains 5 very important parameters that change frequently in runs and tests. 
\begin{lstlisting}[language=Bash]
mpiexec -n 4 ./parallel < ../test_input_parallel.txt
./serial < ../test_input_serial.txt
\end{lstlisting}
The parameters with their types are summarized below. The number are separated by white space, while strings are separated by a semicolon. 
\begin{lstlisting}
Total_Iterations(int) Update_Frequency(int) Max_Delta(Float) input(char[150]):output(char[150])
\\Example:
100 10 0.01"input.pgm":"output.pgm"
\end{lstlisting}
\item[Submitting jobs] \hfil \\
After compiling when one wants to submit sample jobs (job.sge for this purpose in main directory), one can type:
\begin{lstlisting}
cd root
qsub -pe mpi $no_of_proc job.sge 
\end{lstlisting}
\end{description}

\subsection{Scope of Tests on Architectures and MPI Implementations}
The program has been tested on Morar with pgi-mpi, and gnu-mpu. The program was tested also on my personal laptop Intel Dual-Core 1.3GHz with 4GB memory with openmpi 1.7.2. 
\end{document}
