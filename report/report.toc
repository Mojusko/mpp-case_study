\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Design}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Task and Serial Code}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Theory of Parallel Implementation}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Basic Description}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Additional Description}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Practical Issues \& Code Organization}{5}{subsection.2.5}
\contentsline {section}{\numberline {3}Usage}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Requirements}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}How to Build}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}How to Run}{6}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Scope of Tests on Architectures and MPI Implementations}{6}{subsection.3.4}
\contentsline {section}{\numberline {4}Demonstration of Correctness}{7}{section.4}
\contentsline {section}{\numberline {5}Performance Tests}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Note on Performance tests}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Results}{7}{subsection.5.2}
\contentsline {section}{\numberline {6}Conclusion}{7}{section.6}
