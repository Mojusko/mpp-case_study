//---------------------------------------------------------------------------------------------
// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//---------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include "help.h"
#include <math.h>

// Rank = 0 corresponds to Master Process
// Rank = 1 corresponds to Worker Process



int main( void )
{
    MPI_Init(NULL, NULL);
    int rank;
	int size;
	int default_tag=0;
	MPI_Comm comm2;
	MPI_Comm comm;
	comm2 = MPI_COMM_WORLD;
	MPI_Comm_size(comm2, &size);
	int M,N;
	int startm,startn,mmh,nnh,MM,NN,mm,nn;
	int i,j,k,q,z;
	int total_iterations,update_frequency;
	RealNumber error;
	MPI_Comm_rank(comm2, &rank);
	double starttime, endtime;
	double iterstart,iterend;
	double min=1000;
	char input_name[150]; char output_name[150];
	if (rank==0){
		printf("Starting the Program\n");
		printf("--------------------------------------------------------\n");
	}



	//----------------------------------------------
	//------------- Creating 2D Topology -----------
	//----------------------------------------------
	int dims[2];	dims[0]=0; 	dims[1]=0;
	MPI_Dims_create(size,2,dims);
	int periods[2]; periods[0]=0; periods[1]=0;
	MPI_Cart_create(comm2,2,dims, periods,1, &comm);
	MPI_Comm_rank(comm, &rank);
	if (rank==0){
		printf("Topology created: (%d,%d)\n",dims[0],dims[1]);
	}	
	int addresses[4];
	Check_Neighbours(rank,addresses,&comm);
	//----------------------------------------------
	//----------------------------------------------

	if (rank==0){	

		Load_Parameters(&total_iterations,&update_frequency,&error,input_name,output_name);
		//----------------------------------------------------------
		//---------- Parameters & Probing the size of the image-----
		//----------------------------------------------------------
		printf("Probing the Image Size:\n");
	    pgmsize(input_name, &M, &N); // probbing the size of an image
		printf("Done. Dimensions %d, %d\n",M,N);
		printf("--------------------------------------------------------\n");
		//---------------------------------------------------------------
		//------------ Broadcasting the data size to other --------------
		//---------------------------------------------------------------
		MM=M+2;
		NN=N+2;
		int dimensions[4];
		dimensions[0]=MM;
		dimensions[1]=NN;
		dimensions[2]=update_frequency;
		dimensions[3]=total_iterations;
		MPI_Bcast(dimensions,4,MPI_INT,0,comm);
		MPI_Bcast(&error,1,MPI_REALNUMBER,0,comm);
		//---------------------------------------------------------------
		//------------ Split variables Calculation on Master-------------
		//---This calculates which portion of data each node processes---
		//---------------------------------------------------------------
		Optimal_Division(MM,NN,&startm,&startn,&mm,&nn,rank,size,&comm);
	}

	else{
		//--------------------------------------------------------------
		// ------ Receiving dimensions of the image from the Master ----
		//--------------------------------------------------------------
		int dimensions[4];
		MPI_Bcast(dimensions,4,MPI_INT,0,comm);
		MM=dimensions[0];
		NN=dimensions[1];
		update_frequency=dimensions[2];
		total_iterations=dimensions[3];
		MPI_Bcast(&error,1,MPI_REALNUMBER,0,comm);
		printf("Received Dimensions on the rank: %d which are %d, %d\n",rank,MM,NN);
		//---------------------------------------------------------------
		//------------ Split variables Calculation on Worker ------------
		//---This calculates which portion of data each node processes---
		//---------------------------------------------------------------
		Optimal_Division(MM,NN,&startm,&startn,&mm,&nn,rank,size,&comm);
	}


	//-------Creation of local_edge_array
	RealNumber (* edge_worker)[nn] = malloc( sizeof(* edge_worker) * mm ) ;


	//-----------------------------------------------------------------
	//---------------- Reading the Image (I/O)-------------------------
	//-----------------------------------------------------------------
	if (rank==0){
		// ---------Reading data-----------------------------------
		float (* buf)[N] = malloc( sizeof(* buf) * M ) ;

		printf("Reading the data of size : %d,%d on master node\n",M,N);
		pgmread(input_name, buf, M, N);
		printf("Done Reading the data of master node\n");
		printf("--------------------------------------------------------\n");
		//---------------------------------------------------------------
		//----------Setting white borders to the image ------------------
		//---------------------------------------------------------------
		RealNumber (* edge)[NN] = malloc( sizeof(* edge) * (MM) ) ;

		for (i=0;i<M+2;i++){edge[i][0]=255.0;}
		for (i=0;i<M+2;i++){edge[i][N+1]=255.0;}
		for (j=0;j<N+2;j++){edge[0][j]=255.0;}
		for (j=0;j<N+2;j++){edge[M+1][j]=255.0;}
		for (i=1;i<M+1;i++){
			for (j=1;j<N+1;j++){
				edge[i][j]=buf[i-1][j-1];
			}
		}
		//---------------------------------------------------------------
		//------------ Distributign the data from master ----------------
		//---------------------------------------------------------------
		Distribute_Data(MM,NN,edge,size,rank,&comm);
		//---------------------------------------------------------------
		//------------ Split variables Calculation ----------------------
		//---------------------------------------------------------------
		Optimal_Division(MM,NN,&startm,&startn,&mm,&nn,rank,size,&comm);
		//---------------------------------------------------------------
		//------------ Splicing out data for masters node ---------------
		//---------------------------------------------------------------
		Splice_Out(startm,startn,mm,nn,MM,NN,edge,edge_worker);
		free(edge);
	}
	//---------------------------------------------------------------
	//------------ Receiving data on worker node --------------------
	//---------------------------------------------------------------
	else {
		Receive_Data(MM,NN,mm,nn,edge_worker,rank,&comm);
	}
	// Start Timing
	starttime = MPI_Wtime();
	iterstart = MPI_Wtime();
	//---------------------------------------------------------------
	//---------- Setting Initial Conditions before Calculation-------
	//---------------------------------------------------------------
	printf("Setting Initial Conditiosn on rank %d \n",rank);
	mmh=mm+2;
	nnh=nn+2;

	RealNumber (* old)[nnh] = malloc( sizeof(* old) * mmh ) ;
	RealNumber (* new)[nnh] = malloc( sizeof(* new) * mmh ) ;
	
	Set_255(mmh,nnh,old);
	Set_255(mmh,nnh,new);
	printf("Done Setting Initial Conditiosn on rank %d\n",rank);
	printf("--------------------------------------------------------\n");
	//***************************************************************
	// ----------- Performing Calculation ---------------------------
	//***************************************************************
	printf("Calculation Started on rank %d\n",rank);
	//---------------------------------------------------------------
	// ----------- Checking for Padding -----------------------------
	//---------------------------------------------------------------
	int padn,padm,padn_max,padm_max;
	Check_Paddining(MM,NN,mm,nn,startm,startn,&padm,&padn,&padm_max,&padn_max);
	printf("Padding Initialized: %d %d %d %d on Rank: %d \n",padm,padn,padm_max,padn_max,rank);
	//---------------------------------------------------------------
	// ----------- Main Loop ----------------------------------------
	//---------------------------------------------------------------
	int iteration=0; // iteration variable
	RealNumber delta,previous_delta; delta=Delta_init; // Initial condition for delta
	while (Stop_Condition(iteration,rank,delta,&previous_delta,total_iterations,error,update_frequency,&comm)==0){
		delta=0;

		MPI_Request request[4]; // for 4 non-blocking sends
		int calls[4]; // for 4 non-blocking send 
		Send_Halos_Non_Blocking(mmh,nnh,size,rank,request,calls,addresses,old,&comm);
	
		//Iterate over non-halo part of the image
		Iterate_Over_Non_Halo_Part(mm,nn,edge_worker,mmh,nnh,old,new,&delta);
		//---------------------------------------

		Receive_Halos(mmh,nnh,size,rank,addresses,old,&comm);
		Wait_Halos_Non_Blocking(request,calls);

		//Iterate over halo part of the image i,j 2,mmh-2,nnh-2
		Iterate_Over_Halo_Part(mm,nn,edge_worker,mmh,nnh,old,new,&delta,padm,padn,padm_max,padn_max);
		//-------------------------------------------------------

		//Calculate Average and Print it out on Master Rank
		Average(iteration,M,N,mmh,nnh,padm,padn,padm_max,padn_max,new,rank,update_frequency,&comm);
		#ifdef VERBOSE
		if (iteration>1 && iteration%update_frequency==0 && rank==0){
				iterend=MPI_Wtime();
				printf("TIME PER UPDFREQ M: %d N: %d size: %d total_it: %d iter: %d Error: %f time: %f \n",M,N,size,total_iterations,iteration,error,iterend-iterstart);
				if ((iterend-iterstart)<min){
					min=(iterend-iterstart);
				}
				iterstart=MPI_Wtime();

		}
		#endif
		//Update NEW = OLD
		Copy(mmh,nnh,new,old);
		iteration++;
	}



	printf("Calculation Finished on rank: %d\n",rank);
	printf("--------------------------------------------------------\n");
	//***************************************************************
	// ----------- Calculation Finished -----------------------------
	//***************************************************************
	if (rank==0){
			//-------------------------------------------------------------
			//-----------Incroporating partial data to form a big image ---
			//-------------------------------------------------------------
			float (* buf)[N] = malloc( sizeof(* buf) * M ) ;
			RealNumber (* buf2)[NN] = malloc( sizeof(* buf2) * MM ) ;
			
			Set_255(MM,NN,buf2);
			//Master Data
			Insert_to_array(startm,startn,MM,NN,buf2,mmh,nnh,old);
			//Workers Data
			Receive_Others(MM,NN,buf2,size,&comm);
			endtime=MPI_Wtime();
			printf("TIME TO FINISH M: %d N: %d size: %d total_it: %d iter: %d Error: %f time: %f \n",M,N,size,total_iterations,iteration,error,endtime-starttime);
			printf("TIME OF MIN  M: %d N: %d size: %d total_it: %d iter: %d Error: %f time: %f \n",M,N,size,total_iterations,iteration,error,min);
			// ----------  Writting out ---------------
			//Set_255(M,N,buf); // Initial Values
			Cut_Edges(MM,NN,buf2,M,N,buf); // Cut Edges of the image (white halos) and Insert
			pgmwrite(output_name,buf,M,N); // Writting out
			free(buf);
			free(buf2);
	}
	else {
		// Send Back Data to Master
		Send_Data_Back(mmh,nnh,old,&comm);	
	}

	free(new);
	free(old);
	free(edge_worker);
	MPI_Finalize();
	
	return 0;
}
