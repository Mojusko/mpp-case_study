//---------------------------------------------------------------------------------------------
// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//---------------------------------------------------------------------------------------------
#pragma once
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "pgmio.h"
#include "parameters.h"
//--------------------------------------------------------
// ------------- Distribution of Data---------------------
//--------------------------------------------------------
void Distribute_Data(int MM, int NN,RealNumber edge[MM][NN], int size, int rank,MPI_Comm *comm);
void Receive_Data(int MM, int NN,int mm,int nn, RealNumber edge[MM][NN], int rank, MPI_Comm *comm);

//--------------------------------------------------------
// ------------- Gathering Data Back ---------------------
//--------------------------------------------------------
void Receive_Others(int MM,int NN,RealNumber buff2[MM][NN],int size, MPI_Comm *comm);
void Send_Data_Back(int mmh,int nnh,RealNumber buf[mmh][nnh],MPI_Comm *comm);

//--------------------------------------------------------
//------------- Finding out Neighbours in the Grid -------
//--------------------------------------------------------
// Based on the information in the adresses array this procedure return -1 if not a neight burs 
int Is_Neighbour(int rank,int neigh,int adresses[4],MPI_Comm *comm);
// This procedure computes the neighours in all 4 directions on 2D grid
void Check_Neighbours(int rank,int adresses[4],MPI_Comm *comm);

//--------------------------------------------------------
//---------------Sending Halos ---------------------------
//--------------------------------------------------------
// This two procedures send halo to the neighbours in all 4 directions, for each direction, request is created in the array,
// also calls[] contains whether the call has been called (whether it was sent)
void Send_Halos_Non_Blocking(int mmh,int nnh,int size,int rank,MPI_Request request[4],int calls[4],int adresses[4],RealNumber old[mmh][nnh],MPI_Comm *comm);
void Wait_Halos_Non_Blocking(MPI_Request requests[4],int calls[4]);
// In this procedure we receive halos to the array old[] using defined derived type within the procedure
void Receive_Halos(int mmh,int nnh,int size,int rank,int adresses[4],RealNumber old[mmh][nnh],MPI_Comm *comm);
