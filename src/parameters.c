// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//--------------------------------------------------------
#include "parameters.h"
#include <stdio.h>
#include <stdlib.h>

void Load_Parameters(int *total_iterations,int *update_frequency, RealNumber *error,char input_name[150],char output_name[150]){
// there numbers first
scanf("%d", total_iterations);
scanf("%d", update_frequency);
scanf("%f", error);
if (scanf("%150[^:]:%150s", input_name, output_name) == 2)
{
	//printf("%s %s\n",input_name,output_name );
}
else{
	//printf("Unsuccessfull\n");
}

}