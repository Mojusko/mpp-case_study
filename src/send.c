//---------------------------------------------------------------------------------------------
// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//---------------------------------------------------------------------------------------------
#include "send.h" 

//--------------------------------------------------------
// ------------- Distribution of Data---------------------
//--------------------------------------------------------


/* Distribute_Data(int,int,float [][],int,int,MPI_comm*);
This procedure Distributes Part of The image to all the ranks with blocking sends. It is called on Master rank.
It only send the part of the image which are needed for the computiation
*/
void Distribute_Data(int MM, int NN,RealNumber edge[][NN], int size,int rank, MPI_Comm *comm){
	int i,j,k,z;
	// Send Data to each worker Rank:
	for (i=1;i<size;i++){
		z=0;
		int startm,startn,mm,nn;
		//Calculate Split
		Optimal_Division(MM,NN,&startm,&startn,&mm,&nn,i,size,comm);
		RealNumber *msg=malloc( sizeof(RealNumber) * (mm*nn) );
		RealNumber (* edge_splice)[nn] = malloc( sizeof(* edge_splice) * (mm) ) ;

		//Splice Out
		Splice_Out(startm,startn,mm,nn,MM,NN,edge,edge_splice);

		// Copying image to the message
		for (j=0;j<mm;j++){
			for(k=0;k<nn;k++){
				msg[z]=edge_splice[j][k];
				z++;
			}
		}

		// Sending msg
		printf("Sending msg with information to rank: %d\n",i);
		printf("Sending to rank: %d done\n",i);
		printf("--------------------------------------------------------\n");
		MPI_Ssend (msg, mm*nn, MPI_REALNUMBER, i, defaulttag, *comm);

		free(msg);
		free(edge_splice);
	}
}	
/* Receive_Date(int,int,int,int float[][],int,MPI_Comm*);
This function receives the data send by Distribute Data function. It is called from the Worker node. 
It incorporates the data to edge_worker.
*/
void Receive_Data(int MM, int NN,int mm,int nn, RealNumber edge_worker[mm][nn], int rank, MPI_Comm *comm){
	// Send Data to each worker Rank:
	RealNumber *msg=malloc( sizeof(RealNumber) * (mm*nn) );
	//RealNumber msg[mm*nn];
	MPI_Status status;
	printf("Receiving on the rank %d \n",rank);
	MPI_Recv(msg, mm*nn, MPI_REALNUMBER, 0, defaulttag, *comm,&status);
	printf("Received on the rank %d \n",rank);
	printf("--------------------------------------------------------\n");
	int j,k,z;
	z=0;
	// Copying image to the message
	for (j=0;j<mm;j++){
		for(k=0;k<nn;k++){
			edge_worker[j][k]=msg[z];
			z++;
		}
	}
	free(msg);
}
//--------------------------------------------------------
// ------------- Gathering Data Back ---------------------
//--------------------------------------------------------

/* Receive_Others(int,int,float[][],int,MPI_Comm *)
This function is Called on Master rank and gathers data send by worker nodes, when they finish calculation 
*/
void Receive_Others(int MM,int NN,RealNumber buff2[MM][NN],int size, MPI_Comm *comm){
	int i,j,z,q;
	MPI_Status status;

	for (q=1;q<size;q++){
		
		int startm,startn,mm,nn;
		// Optimal division for rank=q
		Optimal_Division(MM,NN,&startm,&startn,&mm,&nn,q,size,comm);

		int mmh=mm+2;
		int nnh=nn+2;

		printf("RECV : mmh %d nnh %d \n",mmh,nnh);
		RealNumber (*msg)=malloc( sizeof(RealNumber) * (mmh*nnh) );
		RealNumber (* buf)[nnh] = malloc( sizeof(* buf) * (mmh) ) ;

		MPI_Recv(msg, mmh*nnh, MPI_REALNUMBER, q, defaulttag, *comm,&status);
		printf("Received Message from %d, and with params %d %d %d %d\n",q,startm,startn,mm,nn);
		
		z=0;
		for (i=0;i<mmh;i++){
			for (j=0;j<nnh;j++){
				buf[i][j]=msg[z];
				z++;
			}
		}
		// Insert to the main array
		Insert_to_array(startm,startn,MM,NN,buff2,mmh,nnh,buf);
		free(msg);
		free(buf);
	}
}

/* Send_Data_Back(int,int,float[][],MPI_Comm*);
This function is called on worker nodes, and send back the data after calculation from buf. 
*/
void Send_Data_Back(int mmh,int nnh,RealNumber buf[mmh][nnh],MPI_Comm *comm){
	int i,j,q;
	q=0;
	printf("SEND BACK: mmh %d nnh %d \n",mmh,nnh);
	RealNumber *msg=malloc( sizeof(RealNumber) * (mmh*nnh) );
	
	for (i=0;i<mmh;i++){
		for (j=0;j<nnh;j++){
			msg[q]=buf[i][j];
			q++;
		}
	}
	MPI_Ssend (msg, (mmh)*(nnh), MPI_REALNUMBER, 0, defaulttag, *comm);
	free(msg);
}

//--------------------------------------------------------
//------------- Finding out Neighbours in the Grid -------
//--------------------------------------------------------

/* Check_Neighbours(int,int[],MPI_Comm*);
This function calculates ranks of the neighbours in all 4 directions
0 - right
1 - up
2 - left
3 - down
*/
void Check_Neighbours(int rank,int addresses[4],MPI_Comm *comm){
	int i,dest;
	MPI_Cart_shift(*comm,0,1 ,&rank, &dest);
	addresses[0]=dest;
	MPI_Cart_shift(*comm,1,1 ,&rank, &dest);
	addresses[1]=dest;
	MPI_Cart_shift(*comm,0,-1,&rank, &dest);
	addresses[2]=dest;
	MPI_Cart_shift(*comm,1,-1,&rank, &dest);
	addresses[3]=dest;
}

/*Is Neighbour(int,int,int[],MPI_Comm *)
	This function analyzes data in adresses, and outputs whether a cells has neighbour in direction (0,1,2,3). 
	Negative number for no neighbour otherwise outputs rank of the neigbour. 
*/
int Is_Neighbour(int rank,int neigh,int adresses[4],MPI_Comm *comm){
	if (adresses[neigh]<0) {
		return -1;
	}
	else {
		return adresses[neigh];
	}
}

//--------------------------------------------------------
//---------------Sending Halos ---------------------------
//--------------------------------------------------------

/* Send_Halos_Non_Blocking(int,int,int,int,MPI_Request[],int[],int[],float[][],MPI_Comm*);
This function send halos of the rank at which it is called. It Uses derived datatype to send a stripe out of an "old" array. First it check whether there
are neighbours using the above functions. Then it sends. These are non-blocking sends, and their request are inserted into request[] array. Also, call[] contains
0/1 whether a call has been called (message has been sent).
*/
void Send_Halos_Non_Blocking(int mmh,int nnh,int size,int rank,MPI_Request request[4],int calls[4],int adresses[4],RealNumber old[mmh][nnh],MPI_Comm *comm){
	int i,j,q,dest,z;

	//----------- Creation of Derived Data Type
	MPI_Datatype Halo_Horizontal;
	MPI_Type_vector(mmh-2,1,nnh,MPI_REALNUMBER,&Halo_Horizontal);
	MPI_Datatype Halo_Vertical;
	MPI_Type_vector(1,nnh-2,nnh*mmh,MPI_REALNUMBER,&Halo_Vertical);
	MPI_Type_commit (&Halo_Horizontal);
	MPI_Type_commit (&Halo_Vertical);
	//-----------------------------------------

	for (q=0;q<4;q++){
		dest=Is_Neighbour(rank,q,adresses,comm);
		if (dest!=-1){
			// The following ifs select 0,1,2,3 by their properties of divison by 2.
			if (q%2==0){
				//0
				if (q/2==0){
					MPI_Issend(&old[mmh-2][1],1,Halo_Vertical,dest,defaulttag,*comm,&request[q]);
				}
				//2
				else{
					MPI_Issend(&old[1][1],1,Halo_Vertical,dest,defaulttag,*comm,&request[q]);
				}
				calls[q]=1;
			}
			if (q%2==1){
				//1
				if (q/2==0){
					MPI_Issend(&old[1][nnh-2],1,Halo_Horizontal,dest,defaulttag,*comm,&request[q]);
				}
				//3
				else{
					MPI_Issend(&old[1][1],1,Halo_Horizontal,dest,defaulttag,*comm,&request[q]);
				}
				calls[q]=1;
			}
		}
		else{
			calls[q]=0;
		}
	}
	MPI_Type_free(&Halo_Horizontal);
	MPI_Type_free(&Halo_Vertical);
}
/* Wait_Halos_Non_Blocking(MPI_request [],int[]);
This is a correspodning wait function for the above non-blocking send.
*/
void Wait_Halos_Non_Blocking(MPI_Request requests[4],int calls[4]){
	int i;
	MPI_Status status;
	for (i=0;i<4;i++){
		if (calls[i]==1){
			MPI_Wait(&requests[i],&status);
		}
	}
}

/* Receive_Halos(int,int,int,int,int[],float[][],MPI_Comm*);
This function receives halos from its neighbours it is very similar to the send function by default. 
*/
void Receive_Halos(int mmh,int nnh,int size,int rank,int adresses[4],RealNumber old[mmh][nnh],MPI_Comm *comm){
	MPI_Status status;
	int q,source,z;

	//----------- Creation of Derived Data Type
	MPI_Datatype Halo_Horizontal;
	MPI_Type_vector(mmh-2,1,nnh,MPI_REALNUMBER,&Halo_Horizontal);
	MPI_Datatype Halo_Vertical;
	MPI_Type_vector(1,nnh-2,nnh*mmh,MPI_REALNUMBER,&Halo_Vertical);
	MPI_Type_commit (&Halo_Horizontal);
	MPI_Type_commit (&Halo_Vertical);
	//-------------------------------------------

	for (q=0;q<4;q++){
		source=Is_Neighbour(rank,q,adresses,comm);
		if (source!=-1){
			// The following ifs select 0,1,2,3 by their properties of divison by 2.
			if (q%2==0){
				//0
				if (q/2==0){
					MPI_Recv(&old[mmh-1][1],1,Halo_Vertical,source,defaulttag,*comm,&status);
				}
				//2
				else{
					MPI_Recv(&old[0][1],1,Halo_Vertical,source,defaulttag,*comm,&status);
				}
			}
			if (q%2==1){
				//1
				if (q/2==0){
					MPI_Recv(&old[1][nnh-1],1,Halo_Horizontal,source,defaulttag,*comm,&status);
				}
				//3
				else{
					MPI_Recv(&old[1][0],1,Halo_Horizontal,source,defaulttag,*comm,&status);
				}
			}



		}
	}
	MPI_Type_free(&Halo_Horizontal);
	MPI_Type_free(&Halo_Vertical);

}

