//---------------------------------------------------------------------------------------------
// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//---------------------------------------------------------------------------------------------
#pragma once
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "parameters.h"
//-----------------------------------------------------------
// --------------- General Helper Functions -----------------
// ----------------------------------------------------------
RealNumber ffmax(RealNumber a, RealNumber b);
void Set_255(int rows,int cols,RealNumber arr[rows][cols]);
void Copy(int rows,int cols,RealNumber new[rows][cols],RealNumber old[rows][cols]);

//-----------------------------------------------------------
// -------------- Array Manipulation Functions --------------
//-----------------------------------------------------------
void Splice_Out(int startm, int startn,int mm,int nn,int MM,int NN,RealNumber edge[MM][NN],RealNumber edge_master[mm][nn]);
void Insert_to_array(int startm, int startn, int MM,int NN,RealNumber buff2[MM][NN],int mmh,int nnh,RealNumber old[mmh][nnh]);
void Cut_Edges(int MM,int NN,RealNumber buff2[MM][NN],int M,int N,float buff[M][N]);
void Check_Paddining(int MM,int NN,int mm,int nn,int startm,int startn,int *padm,int *padn,int *padm_max,int *padn_max);

//-----------------------------------------------------------
// ------------- Optimal Division Routine -------------------
//-----------------------------------------------------------
void Optimal_Division(int MM,int NN,int *startm,int *startn,int *mm, int *nn,int rank,int size,MPI_Comm *comm);

//-----------------------------------------------------------
// -------------- Stopping Condition ------------------------
//-----------------------------------------------------------
int Stop_Condition(int iter,int rank, RealNumber change,RealNumber *previous, int total_iterations, RealNumber error,int update_frequency,MPI_Comm *comm);

//-----------------------------------------------------------
// -------------- Averaging the image -----------------------
//-----------------------------------------------------------
void Average(int iter,int M,int N,int mmh,int nnh,int padm, int padn,int padm_max,int padn_max,RealNumber new[mmh][nnh],int rank,int update_frequency,MPI_Comm *comm);

//-----------------------------------------------------------
// -------------- Iteration over image of Jacobi-Algorithm -- 
//-----------------------------------------------------------
void Iterate_Over_Halo_Part(int mm,int nn,RealNumber edge_worker[mm][nn],int mmh,int nnh,RealNumber old[mmh][nnh],RealNumber new[mmh][nnh],RealNumber *delta,int padm,int padn,int padm_max,int padn_max);
void Iterate_Over_Non_Halo_Part(int mm,int nn,RealNumber edge_worker[mm][nn], int mmh,int nnh,RealNumber old[mmh][nnh],RealNumber new[mmh][nnh],RealNumber *delta);
