//---------------------------------------------------------------------------------------------
// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//---------------------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "parameters.h"

int total_iterations,update_frequency;
RealNumber error;
char input_name[150]; char output_name[150];

RealNumber ffmax(RealNumber a,RealNumber b){
	if (a>b) {return a;
	}
	else {return b;}
}

// ---------------------------------------------------------------------
// Basic Serial Implementation of Image Reconstruction from its Laplcian 
// ---------------------------------------------------------------------
// Parameters are taken from file parameters.h
//
int main(void)
{
	int M,N;
	int nx, ny;
    Load_Parameters(&total_iterations,&update_frequency,&error,input_name,output_name);
    pgmsize(input_name, &nx, &ny);
	M=nx;
	N=ny;
	printf("Program Starts.\n");
	// ---------Reading--------
	//float buf[M][N];
	float (* buf)[N] = malloc( sizeof(* buf) * M ) ;
	printf("Dimensions of the Image: %d,%d\n",M,N);
	pgmread(input_name, buf, M, N);
	//------------------------
	int MM=M+2;
	int NN=N+2;
	RealNumber (* edge)[NN] = malloc( sizeof(* edge) * (MM) ) ;
	int i,j;
	printf("Image Read.\n");
	//------------Setting white halos halos -----------
	for (i=0;i<M+2;i++){edge[i][0]=255.0;}
	for (i=0;i<M+2;i++){edge[i][N+1]=255.0;}
	for (j=0;j<N+2;j++){edge[0][j]=255.0;}
	for (j=0;j<N+2;j++){edge[M+1][j]=255.0;}
	for (i=1;i<M+1;i++){
		for (j=1;j<N+1;j++){
			edge[i][j]=buf[i-1][j-1];
		}
	}
	printf("White Borders Set.\n");
	//------------Setting Initial conditions----------
	RealNumber (* old)[NN] = malloc( sizeof(* old) * (MM) ) ;
	RealNumber (* new)[NN] = malloc( sizeof(* new) * (MM) ) ;

	for (i=0;i<M+2;i++) {
		for (j=0;j<N+2;j++){
			old[i][j]=255;
			new[i][j]=255;
		}
	}
	printf("Initial Conditions set.\n");
	//-----------------------------------
	//----- Calculation-----------------
	printf("Calculation Starts:\n");
	int iter;
	RealNumber change;
	for (iter=0;iter<total_iterations;iter++){
		change=0;
		//single iteration
		for (i=1;i<M+1;i++) {
			for (j=1;j<N+1;j++){
				new[i][j]=(1.0/4.0)*(old[i-1][j]+old[i+1][j]+old[i][j-1]+old[i][j+1]-edge[i][j]);
				change=ffmax(change,fabs(new[i][j]-old[i][j]));
			}
		}

		if (iter%update_frequency==0) {
			// ----  Average
				RealNumber Average=0;
			for (i=1;i<M+1;i++){
				for (j=1;j<N+1;j++){
					Average+=(1/(RealNumber)(N*M))*new[i][j];
				}
			}
			#ifdef VERBOSE
				printf("Iteration: %d  Average: %F Max Change: %f\n",iter,Average,change);
			#endif
		}
		//update new=old without halos
		for (i=0;i<M+2;i++){
			for (j=0;j<N+2;j++){
				old[i][j]=new[i][j];
			}
		}
	}
	printf("Calculation Ended.\n");
	//---------------------------------
	//------ Writting out 
	for (i=1;i<M+1;i++){
		for (j=1;j<N+1;j++){
			buf[i-1][j-1]=old[i][j];
		}
	}

	pgmwrite(output_name,buf,M, N);
	free(edge);
	free(old);
	free(new);
	return 0;
}
