//---------------------------------------------------------------------------------------------
// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//---------------------------------------------------------------------------------------------
#include "help.h"
#include <math.h>


//-----------------------------------------------------------
// --------------- General Helper Functions -----------------
// ----------------------------------------------------------

// General Max Function
RealNumber ffmax(RealNumber a, RealNumber b){
	if (a>b) {return a;}
	else {return b;}
}


// Sets Image to value 255
void Set_255(int rows,int cols,RealNumber arr[rows][cols]){
	int i,j;
	for (i=0;i<rows;i++) {
		for (j=0;j<cols;j++){
			arr[i][j]=255.0;
		}
	}
}

// Copies one Array to another
void Copy(int rows,int cols,RealNumber new[rows][cols],RealNumber old[rows][cols]){
	int i,j;
	for (i=0;i<rows;i++){
		for (j=0;j<cols;j++){
			old[i][j]=new[i][j];
		}
	}
}

//-----------------------------------------------------------
// -------------- Array Manipulation Functions --------------
//-----------------------------------------------------------

// ----- Splices out part of an array edge to edge master 
// Starting coordinate (startm,startn) with displacement vector (mm,nn)
// MM,NN are the dimensions of the whole edge image
void Splice_Out(int startm, int startn,int mm,int nn,int MM,int NN,RealNumber edge[MM][NN],RealNumber edge_master[mm][nn]){
	int i,j;
	for (i=startm;i<startm+mm;i++){
		for (j=startn;j<startn+nn;j++){
			edge_master[i-startm][j-startn]=edge[i][j];
		}
	}
}

//----- This function Inserts an array "old" to the "buff2", it also shifts it, not to include outher halos
void Insert_to_array(int startm,int startn,int MM,int NN,RealNumber buff2[MM][NN],int mmh,int nnh,RealNumber old[mmh][nnh]){
	int i,j;
	for (i=1;i<mmh-1;i++){
		for (j=1;j<nnh-1;j++){
			buff2[i+startm-1][j+startn-1]=old[i][j];
		}
	}
}

// --- This functions cuts the end of the array;
void Cut_Edges(int MM,int NN,RealNumber buff2[MM][NN],int M,int N,float buff[M][N]){
	int i,j;
	for (i=1;i<MM-1;i++){
		for (j=1;j<NN-1;j++){
			buff[i-1][j-1]=buff2[i][j];
		}
	}
}
//---- This function check padding
// Calculates whether halo is part of the split. One should read description of this in the report, because it is a difficult concept to explain 
// without a picture. Iteration is performed over the image not over white halos added. To ensure this we calculate this padding. 
void Check_Paddining(int MM,int NN,int mm,int nn,int startm,int startn,int *padm,int *padn,int *padm_max,int *padn_max) {
	*padn=0;*padm=0;*padn_max=0;*padm_max=0;
	if (startm==0){*padm=1;}
	if (startn==0){*padn=1;}
	if (startm+mm==MM){*padm_max=-1;}
	if (startn+nn==NN){*padn_max=-1;}
}

//-----------------------------------------------------------
// ------------- Optimal Division Routine -------------------
//-----------------------------------------------------------
// ---- This function divides the image into "size" parts 
// ---- outputs coordinate that selects beggining of the section (startm,startn) and displacement vector (mm,nn)
void Optimal_Division(int MM,int NN,int *startm,int *startn,int *mm, int *nn,int rank,int size,MPI_Comm *comm){
	//---------- Rank -> Coordinates ------------------- 
	int coords[2]; coords[0]=0; coords[1]=0;
	MPI_Cart_coords(*comm,rank, 2, coords);
	//---------Getting Dimensions of the grid ----------
	int dims[2];	dims[0]=0; 	dims[1]=0;
	MPI_Dims_create(size,2,dims);
	//--------------------------------------------------
	// Spliting M side of the image
	if (MM%dims[0]==0){ // divisible
		*mm=MM/dims[0];
		*startm=(*mm*coords[0]);
	}
	else  { // nondivisible
		if (coords[0]==0) {
			*mm=MM/dims[0]+MM%dims[0];
			*startm=0;
		}
		else{
			*mm=MM/dims[0];
			*startm=*mm*(coords[0])+MM%dims[0];
		}
	}

	if (NN%dims[1]==0){ // divisible
		*nn=NN/dims[1];
		*startn=(*nn*coords[1]);
	}
	else  { // nondivisible
		if (coords[1]==0) {
			*nn=NN/dims[1]+NN%dims[1];
			*startn=0;
		}
		else{
			*nn=NN/dims[1];
			*startn=*nn*(coords[1])+NN%dims[1];
		}
	}
	//printf("SPLITTING on rank: (%d,%d) equates to startm: %d startn: %d, mm: %d nn: %d \n",coords[0],coords[1],*startm,*startn,*mm,*nn);
}

//-----------------------------------------------------------
// -------------- Stopping Condition ------------------------
//-----------------------------------------------------------
// This function outputs, is a stopping criterion function that selects whether the algorithm should be stopped
// Two Modes could be selected
// 1. ITER - iteration, total iteration of some number has to be performed
// 2. ERROR - delta as defined in coursework has to be smaller than some error
// The delta is updated every update_frequency iterations which can be set
//-------
int Stop_Condition(int iter,int rank,RealNumber change,RealNumber *previous,int total_iterations, RealNumber error,int update_frequency, MPI_Comm *comm){
	RealNumber max;

	#ifdef ITER
		if (iter<total_iterations){
			return 0;
		}
		else {return 1;}

	#else 

		#ifdef ERROR
			if (iter%update_frequency==0){
				// We perform reduction Operation to know the maximum change
				MPI_Allreduce(&change,&max,1,MPI_REALNUMBER,MPI_MAX,*comm);
				if (rank==0) { 
					#ifdef VERBOSE
						printf("GLOBAL DELTA: %f\n",max);
						printf("----------------------------------------------------------------\n");
					#endif
				}
				*previous=max;
			}
			
			if (*previous<error){
				return 1;
			}
			else {
				return 0;
			}
		#endif
	#endif 
}



//-----------------------------------------------------------
// -------------- Averaging the image -----------------------
//-----------------------------------------------------------
//-------- This function calculates the average value of the image every update_frequency iterations.
// When VERBOSE define it is printed every update_frequency.
void Average(int iter,int M,int N,int mmh,int nnh,int padm,int padn,int padm_max,int padn_max,RealNumber new[mmh][nnh],int rank,int update_frequency, MPI_Comm *comm){
	int i,j;
	RealNumber Average,Sum;	
	RealNumber divisor;
	// Every Update_Frequency
	if (iter%update_frequency==0){
		// We Calculate Sum of Each node
		Sum=0;
		for (i=1+padm;i<mmh-1+padm_max;i++) {
			for (j=1+padn;j<nnh-1+padn_max;j++){
				Sum+=new[i][j];
			}
		}
		// Now we perform Global Reduction Operation
		MPI_Reduce(&Sum,&Average,1,MPI_REALNUMBER,MPI_SUM,0,*comm);

		// Printing out the Average on Master node
		if (rank==0){
			RealNumber divisor;
			divisor=1/(RealNumber)(N*M);
			#ifdef VERBOSE
				printf("On Iteration: %d we have Average: %f \n",iter,Average*divisor);
			#endif
		}
	}
}

//-----------------------------------------------------------
// -------------- Iteration over image of Jacobi-Algorithm -- 
//-----------------------------------------------------------
// Iteration of the algorithm over non-halo part of the image
void Iterate_Over_Halo_Part(int mm,int nn,RealNumber edge_worker[mm][nn],int mmh,int nnh,RealNumber old[mmh][nnh],RealNumber new[mmh][nnh],RealNumber *delta,int padm,int padn,int padm_max,int padn_max){
	int i,j;
	i=1+padm;
	for (j=padn+1;j<nnh-1+padn_max;j++){
		new[i][j]=(1.0/4.0)*(old[i-1][j]+old[i+1][j]+old[i][j-1]+old[i][j+1]-edge_worker[i-1][j-1]);
		*delta=ffmax(fabs(new[i][j]-old[i][j]),*delta);
	}
	i=mmh-2+padm_max;
	for (j=padn+1;j<nnh-1+padn_max;j++){
		new[i][j]=(1.0/4.0)*(old[i-1][j]+old[i+1][j]+old[i][j-1]+old[i][j+1]-edge_worker[i-1][j-1]);
		*delta=ffmax(fabs(new[i][j]-old[i][j]),*delta);
	}
	j=1+padn;
	for (i=padm+1;i<mmh-1+padm_max;i++){
		new[i][j]=(1.0/4.0)*(old[i-1][j]+old[i+1][j]+old[i][j-1]+old[i][j+1]-edge_worker[i-1][j-1]);
		*delta=ffmax(fabs(new[i][j]-old[i][j]),*delta);
	}
	j=nnh-2+padn_max;
	for (i=padm+1;i<mmh-1+padm_max;i++){
		new[i][j]=(1.0/4.0)*(old[i-1][j]+old[i+1][j]+old[i][j-1]+old[i][j+1]-edge_worker[i-1][j-1]);
		*delta=ffmax(fabs(new[i][j]-old[i][j]),*delta);
	}
	//-------------------------------------------------------
}

// Iteration of the algorithm over the halo part of the image (part which needs halo values)
void Iterate_Over_Non_Halo_Part(int mm,int nn,RealNumber edge_worker[mm][nn], int mmh,int nnh,RealNumber old[mmh][nnh],RealNumber new[mmh][nnh],RealNumber *delta){
	int i,j;
	for (i=2;i<mmh-2;i++) {
		for (j=2;j<nnh-2;j++){
			new[i][j]=(1.0/4.0)*(old[i-1][j]+old[i+1][j]+old[i][j-1]+old[i][j+1]-edge_worker[i-1][j-1]);
			*delta=ffmax(fabs(new[i][j]-old[i][j]),*delta);	
		}
	}
}