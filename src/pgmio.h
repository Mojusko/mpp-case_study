void pgmsize (const char *filename, int *nx, int *ny);
void pgmread (const char *filename, void *vx, int nx, int ny);
void pgmwrite(const char *filename, void *vx, int nx, int ny);
