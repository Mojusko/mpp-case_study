// Name: Reconstruction of Image from its Laplacian in Parallel using MPI, and 2D decomposition
// Date: Oct - Nov 2014
// Purpose: Assesment for MPP Course
//--------------------------------------------------------

// ----------------------------------------------------------
// --- This file contains variable constants ----------------
// ----------------------------------------------------------

// TYPE DEFINITION --------------------
typedef float RealNumber;
#define MPI_REALNUMBER MPI_FLOAT
//typedef double RealNumber;
//#define MPI_REALNUMBER MPI_DOUBLE
//-------------------------------------

//--------- MODE ----------------------
// Comment out the one you do not want
#define ITER //---- Iteration mode, stops after Total Iterations
//#define ERROR // --- Error mode, stops when change is smaller than error
#define VERBOSE // ---- Define if you want I/O during calculation

 
//------- NUMBER CONSTANTS -----------
//static int const total_iterations = 100;
//static RealNumber const error = 0.1;
//static int const update_frequency=200;
static RealNumber const Delta_init=100;
static int const defaulttag=0;

// ------ FUNCTIONS ---------------------
void Load_Parameters(int *total_iterations,int *update_frequency, RealNumber *error,char input_name[150],char output_name[150]);
