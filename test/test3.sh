#Test 2 tries to prove performance of the program
declare -a Images=("`pwd`/../data/edge192x128.pgm" "`pwd`/../data/edge512x384.pgm" "`pwd`/../data/edge768x768.pgm" "`pwd`/../data/edge_success2550x1695.pgm")
#!/bin/bash 
Total_Iterations=-2
Processors=1
while [ $Processors -lt 25 ]
do
	for name in "${Images[@]}"
	do
		#TimeStamp
		dd=`date +%s%N`
		#Names of ithe output files
		ddp=../results_performance/$dd"p.pgm"
		#Create Configuration Files
		echo $Total_Iterations 100 0.001$name:$ddp > "input"$dd"p.txt"
		#Processing 
		echo The counter is $Total_Iterations and Processors $Processors and name $name
		#Create a Job File
		cp ../performance.sge "job"$dd".sge"
		#Ammend Job File
		sed -i 's/inputp.txt/input'$dd'p.txt/g' job${dd}.sge
		#Submit 
		qsub -pe mpi $Processors "job"$dd".sge"
		done		
	let Processors=Processors+1
done
