#This is test Number 1 to prove correctness of the code
#The code generates files to compare
declare -a Images=("`pwd`/../data/edge192x128.pgm" "`pwd`/../data/edge512x384.pgm" "`pwd`/../data/edge768x768.pgm")
#!/bin/bash 
Total_Iterations=100
while [ $Total_Iterations -lt 10000 ]
do
	Processors=1
	while [ $Processors -lt 18 ]
	do
		for name in "${Images[@]}"
		do
			#TimeStamp
			dd=`date +%s%N`
			#Names of output files
			#serial output
			dds=../results_correctness/$dd"s.pgm"
			#paralell output
			ddp=../results_correctness/$dd"p.pgm"
			#Create Configuration Files
			echo $Total_Iterations 200 0.1$name:$dds > "input"$dd"s.txt"
			echo $Total_Iterations 200 0.1$name:$ddp > "input"$dd"p.txt"
			#Processing 
			echo The counter is $Total_Iterations and Processors $Processors and name $name
			#Create a Job File
			cp ../main.sge "job"$dd".sge"
			#Ammend Job File
			sed -i 's/inputs.txt/input'$dd's.txt/g' job${dd}.sge
			sed -i 's/inputp.txt/input'$dd'p.txt/g' job${dd}.sge
			#Submit 
			qsub -pe mpi $Processors "job"$dd".sge"
			done		
		let Processors=Processors+1
	done
	let Total_Iterations=Total_Iterations*10 
done
